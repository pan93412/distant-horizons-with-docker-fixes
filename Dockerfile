FROM ghcr.io/graalvm/jdk-community:21

WORKDIR /home/build/

RUN microdnf install -y git findutils

COPY ./gradlew .
RUN chmod +x ./gradlew
CMD echo "\r========== [CLEAN: $MC_VER] ==========" && \
    ./gradlew clean -PmcVer="$MC_VER" --gradle-user-home .gradle-cache/ && \
    echo "\r========== [BUILD: $MC_VER] ==========" && \
    ./gradlew build -PmcVer="$MC_VER" --gradle-user-home .gradle-cache/ && \
    echo "\r========== [MERGE: $MC_VER] ==========" && \
    ./gradlew mergeJars -PmcVer="$MC_VER" --gradle-user-home .gradle-cache/ && \
    echo "\r========== [DONE:  $MC_VER] =========="
